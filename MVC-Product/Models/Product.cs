﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC_Product.Models
{
    public class Product
    {

        public int Id { get; set; }
        [Required]
        [Display(Name ="دسته بندی")]
        public int? CategoryId { get; set; }
        public virtual Category Category { get; set; }
        [Required]
        [Display(Name = "شرکت سازنده")]
        public int? CompanyId { get; set; }
        public virtual Company Company { get; set; }
        [Required]
        [Display(Name = "نام مدل")]
        public string ModelName { get; set; }
        [Display(Name = "توضیحات")]
        public string Description { get; set; }

    }
}