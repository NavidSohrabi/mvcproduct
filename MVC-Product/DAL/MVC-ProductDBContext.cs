﻿using MVC_Product.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MVC_Product.DAL
{
    public class MVC_ProductDBContext : DbContext
    {
        public MVC_ProductDBContext()
          : base("name=MVC_ProductDBContext")
        {

        }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<Product> Products { get; set; }
        
    }
}